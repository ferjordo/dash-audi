var user="", pass="";
const sitioUrl="https://audiguayaquil.com/SERVICE/";
$(document).ready(function(){
  $('#txt-user').val("");
  $('#txt-pass').val("");
  var sesionAbiertay = 0;
    //localStorage.removeItem("sesionAbierta");
    var sesionAbiertay = localStorage.getItem("sesionAbierta");

    console.log("Sesion abierta al cargar login: "+sesionAbiertay);
  //var sesionAbierta = localStorage.getItem("sesionAbierta");
  if(localStorage.getItem("sesionAbierta")==1){
    //Redirigir al Dashboard
    //Login();
    window.location.href='/dash';
  }
  else {

    console.log("false sesion");
  }

  $(".favicon").attr("href",sitioUrl+"img-dashboard/favicon-32x32.png");
});

//Animar botones al dar click
$( "button" ).click(function() {
  $( this ).append("<span class='aui-response aui-response--masked is-animating'><span class='aui-response__effect aui-response__effect--large' style='left: 50%; top: 50%;'></span></span>");
  //Eliminar luego de animar
  var id= this.id;
  setTimeout(function(){
      $("#"+id+" span" ).remove();//Eliminar
  },1000);//Esperar 1 segundo antes de eliminar
});

var idUsuarioLogin;

function VerificarPass(uidx){
  //Aqui tomar las dos nuevas contraseñajaxSend
  var pass1=$('#txt-pass-nueva1').val();
  var pass2=$('#txt-pass-nueva2').val();
  if(pass1.length<6||pass2.length<6){
    MostrarMensaje("spanPassNueva","aui-textfield__error","Las contraseñas deben tener mínimo 6 caracteres");
  }
  else{
    if(pass1!=pass2){
      MostrarMensaje("spanPassNueva","aui-textfield__error","Las contraseñas no coinciden");
    }
    else{
      //Mandar a actualizar la contra nueva
      console.log("Actualizando nueva contraseña...");
      ActualizarPassFB(uidx,pass1);
    }
  }
}

function ActualizarPassFB(uid,nuevaPass){
  $.post( "/actualizarpassfb",
    { uid: uid,
      pass: nuevaPass })
    .done(function(){
      console.log("Si se envio al index.js el post actualizar");
      ActualizarPassBD(uid,nuevaPass);
    })
    .fail(function() {
      console.log("No se envio al index.js el post a actualizar");
    });
}

function ActualizarPassBD(uid,nuevaPass){
  //POST
  //POST
  $.post(sitioUrl+"usuario-password-login.php",{
    uid: uidx,
    pass: nuevaPass
   })
    .done(function( data ) {
      if(data.estado==3){//Exito al cambiar contraseña
        console.log("Se cambió la contraseña en la base con éxito")
        //MostrarAlerta("msg_perfil","success",data.mensaje);
        //editarPassFirebase(nueva);
        MostrarMensaje("spanPassNueva","aui-textfield__valid","Cambio existoso. Redirigiendo al login...");
        setTimeout(function(){
            window.location.href='/';
        },2000);//Esperar 2 segundos antes de cambiar de pantalla

      }
      else{
      //  MostrarAlerta("msg_perfil","warning",data.mensaje);
      MostrarMensaje("spanPassNueva","aui-textfield__error",data.mensaje);
      }
    })
    .fail(function() {
      //MostrarAlerta("msg_perfil","danger","Ocurrió un error al intentar actualizar los datos");
        MostrarMensaje("spanPassNueva","aui-textfield__error","Ocurrió un error al intentar actualizar la contraseña");
    });
}

function RecuperarClave(){
  //Guardar valores del formulario
  var correo = $('#txt-correo').val();
  //Post para recuper contraseña con el servicio
  $.post( "https://audiguayaquil.com/SERVICE/comprobar-correo.php",
    { correo: correo })
    .done(function( data ) {
      if(data.estado==1){//Correo si esta registrado
        alert("Revise el enlace de recuperación enviado a su correo electrónico. Luego ingrese nuevamente");
        //Redirigir al Log in
        window.location.href='/';
      }
      else if (data.estado==0){// Correo no registrado
        MostrarMensaje("spanCorreo","aui-textfield__error","Este correo electrónico no está registrado en este sitio web");
      }
    })
    .fail(function() {
      MostrarMensaje("spanCorreo","aui-textfield__error","Ocurrió un error al intentar ingresar al sitio Web" );
    });
}

function Login(){

    //if(localStorage.getItem("sesionAbierta")){
  if(localStorage.getItem("sesionAbierta")==1){
    user= localStorage.getItem("usuario",user);
    pass= localStorage.getItem("pass",pass);
  }
  //Guardar valores del formulario
  if($('#txt-user').val()!=""){
    user = $('#txt-user').val();
  }
  if($('#txt-pass').val()!=""){
    pass = $('#txt-pass').val();
  }
  console.log(" user:"+user+"pass:"+pass);
  //Post para logearme con el servicio
  $.post('https://audiguayaquil.com/SERVICE/log-in.php',{
      user_FB: user,
      user_pass: pass,
      navegador: 1 //solo para web
    })
    .done(function( data ) {
      console.log("DONE CONEXION CON LOGIN. estado:"+data.estado+" user:"+user+"pass:"+pass);

      if(data.estado==1){//Usuario y contraseña correcta
        if(data.tipo==1){// tipo 1 es Administrador

        //Guardar datos en Local Storage
        localStorage.setItem("nombre",data.name);//Recibo Name, nombre usa en App
        localStorage.setItem("apellido",data.apellido);
        localStorage.setItem("correo",data.correo);
        localStorage.setItem("telefono",data.telefono);
        localStorage.setItem("imgUrl",data.imagen);
        localStorage.setItem("tokenw",data.tokenw);
        localStorage.setItem("usuario",user);
        localStorage.setItem("pass",pass);
        localStorage.setItem("tipo",data.tipo);
        localStorage.setItem("id",data.id);
        localStorage.setItem("sesionAbierta",1);
        localStorage.setItem("UID",data.uid);
        console.log("UID: "+data.uid);
        //localStorage.setItem("sesionAbierta",true);
        //Redirigir al Dashboard
        window.location.href='/dash';
        return;
        }
        else{
          MostrarMensaje("spanLogin","aui-textfield__error",("Este servicio es solo para administradores. "+
            "Si eres uno, por favor, ingresa con las credenciales de administrador"));
        //  alert("Este servicio es solo para administradores. Si eres uno, por favor, ingresa con las credenciales de administrador")
        }
      }
      else if (data.estado==2){ //Contraseña equivocada
        if($('#txt-user').val()==""||$('#txt-pass').val()==""){
          alert("Debe volver a iniciar sesión porque la contraseña ya no es la misma");
        }
        else{
          MostrarMensaje("spanLogin","aui-textfield__error","Contraseña incorrecta")
        }

      }
      else if(data.estado==0){
        MostrarMensaje("spanLogin","aui-textfield__valid",data.mensaje);
      }
    })
    .fail(function(e) {
      MostrarMensaje("spanLogin","aui-textfield__error","Ocurrió un error al intentar ingresar al sitio Web" )
     });
  }

function MostrarMensaje(span,clase,mensaje){
  $("#"+span).removeClass("aui-textfield__error");
  $("#"+span).removeClass("aui-textfield__valid");
  $("#"+span).addClass(clase);
  $("#"+span).text(mensaje)
  $("#"+span).fadeIn(300,function(){
    setTimeout(function(){
      $("#"+span).fadeOut(1000);//Ocultar mensaje
    },3000);//Esperar 2 segundos antes de ocultar mensaje
  });
}
