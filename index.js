var express = require('express')
  , path = require('path');
var app = express();

///https
/*
const https = require('https');
https.get('https://dashboard-dashboard.1d35.starter-us-east-1.openshiftapps.com/',
res=>{
  let body='';
  res.on('data',data=>{
    body+=data;
  })
  res.on('end', () =>console.log(body));
}).on('error',error=> console.error(error.message));
*/
//FIREBASE

var admin = require("firebase-admin");
var serviceAccount = require("concierge-audi-firebase-adminsdk-0563f-cecf9a0184");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://concierge-audi.firebaseio.com"
});


//FIN FIREBASE

//Para correr en openshift
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
var  ip   = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

//Para recibir post desde otro archivo
var bodyParser = require('body-parser');
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));


app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));// esto sirve para que al poner / en un SRC ya sepa que es public

app.get('/', function(req, res) {
    res.render('login');
});
app.get('/dash', function (requiere, res) {
  res.render('dash');
})

app.get('/passCorreo', function (requiere, res) {
  res.render('passCorreo');
});

app.get('/recuperarpass/:uidtoken', (req, res)=>{
  var uidx=req.params["uidtoken"];
  console.log (uidx);
  res.render('passNueva', {
    uidx:uidx
  });
//  res.end("Se ha recibido el token:"+uid);
});


//const http = require('http');
/*
app.get('/', function(req, res) {
    //res.render('login');
    res.end("TEST AUDI SIN MODULOS");

});
*/

//LEVANTAR LA APP
app.listen(port, ip,function(){
  //console.log('Server running on http://%s:%s', ip, port);
  console.log("LISTENING");
});

app.post('/delete',  (req, res) => {
  console.log(req.body.uid);

  admin.auth().deleteUser(req.body.uid)
    .then(function() {
      console.log("Successfully deleted user");
      res.end();
    })
    .catch(function(error) {
      console.log("Error deleting user:", error);
    });
    res.end();
});

app.post('/actualizarpassfb',  (req, res) => {
  var uid = (req.body.uid);
  var pass =(req.body.pass);

  admin.auth().updateUser(uid, {
  password: pass
})
  .then(function(userRecord) {
    // See the UserRecord reference doc for the contents of userRecord.
    console.log("Successfully updated user", userRecord.toJSON());
  })
  .catch(function(error) {
    console.log("Error updating user:", error);
  });
    res.end();
});
app.post('/actualizarcorreofb',  (req, res) => {
  var uid = (req.body.uid);
  var correo =(req.body.correo);

  admin.auth().updateUser(uid, {
  email: correo
})
  .then(function(userRecord) {
    // See the UserRecord reference doc for the contents of userRecord.
    console.log("Successfully updated user", userRecord.toJSON());
  })
  .catch(function(error) {
    console.log("Error updating user:", error);
  });
    res.end();
});

module.exports = app ;
